# Towers of Hanoi
#
# Write a Towers of Hanoi game:
# http://en.wikipedia.org/wiki/Towers_of_hanoi
#
# In a class `TowersOfHanoi`, keep a `towers` instance variable that is an array
# of three arrays. Each subarray should represent a tower. Each tower should
# store integers representing the size of its discs. Expose this instance
# variable with an `attr_reader`.
#
# You'll want a `#play` method. In a loop, prompt the user using puts. Ask what
# pile to select a disc from. The pile should be the index of a tower in your
# `@towers` array. Use gets
# (http://andreacfm.com/2011/06/11/learning-ruby-gets-and-chomp.html) to get an
# answer. Similarly, find out which pile the user wants to move the disc to.
# Next, you'll want to do different things depending on whether or not the move
# is valid. Finally, if they have succeeded in moving all of the discs to another pile,
# they win! The loop should end.
#
# You'll want a `TowersOfHanoi#render` method. Don't spend too much time on
# this, just get it playable.
#
# Think about what other helper methods you might want. Here's a list of all the
# instance methods I had in my TowersOfHanoi class:
# * initialize
# * play
# * render
# * won?
# * valid_move?(from_tower, to_tower)
# * move(from_tower, to_tower)
#
# Make sure that the game works in the console. There are also some specs to
# keep you on the right track:
#
# ```bash
# bundle exec rspec spec/towers_of_hanoi_spec.rb
# ```
#
# Make sure to run bundle install first! The specs assume you've implemented the
# methods named above.

class TowersOfHanoi
  attr_reader :towers

  def initialize(towers = [[3,2,1], [], []])
    @towers = towers
  end

  def play
    count = 0
    until won?
      render
      game
      count += 1
    end
    puts "You won the game! Congrats! It took you #{count} steps to get there!"
  end

  def game
    first_tower = first_assignment
    puts 'Please provide which tower to give a piece to.'
    second_tower = gets.chomp.to_i
    if valid_move?(first_tower, second_tower)
      move(first_tower, second_tower)
    else
      puts 'Invalid response(s). Restarting.'
      game
    end
  end

  def first_choice_ok?(from_tower)
    return false if ![0,1,2].include?(from_tower) || towers[from_tower].empty?
    true
  end

  def render
    puts @towers.map {|tower| tower[2] ? "#{picture(tower[2])}" : "     "}.join(" ")
    puts @towers.map {|tower| tower[1] ? "#{picture(tower[1])}" : "     "}.join(" ")
    puts @towers.map {|tower| tower[0] ? "#{picture(tower[0])}" : "     "}.join(" ")
    puts "_____ _____ _____"
  end

  def picture(value)
    if value ==  1
      '  -  '
    elsif value == 2
      ' --- '
    elsif value == 3
      '-----'
    end
  end

  def first_assignment
    puts 'Please provide which tower to obtain a piece from.'
    answer = gets.chomp.to_i
    if first_choice_ok?(answer)
      answer
    else
      puts 'Invalid response.'
      first_assignment
    end
  end

  def move(from_tower, to_tower)
    piece = @towers[from_tower].pop
    @towers[to_tower].push(piece)
  end

  def valid_move?(from_tower, to_tower)
    piece = @towers[from_tower].last
    if piece.nil? || to_tower > 2
      return false
    elsif @towers[to_tower].empty? || @towers[to_tower].last.to_i > piece
      return true
    end
    false
  end

  def won?
    return true if @towers[1] == [3,2,1] || @towers[2] == [3,2,1]
    false
  end
end

if __FILE__ == $PROGRAM_NAME
  t = TowersOfHanoi.new
  t.play
end
